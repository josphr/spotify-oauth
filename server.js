import express from 'express'
import request from 'request'
import querystring from 'querystring'

const app = express()

const redirect_uri = 
    process.env.REDIRECT_URI || 
    'http://localhost:8888/callback'


app.get('/login',(req,res) => {
    res.redirect('https://accounts.spotify.com/authorize?' + 
        querystring.stringify({
            response_type : 'code',
            client_id : process.env.SPOTIFY_CLIENT_ID,
            scope : ['user-read-private user-read-email playlist-read-private user-top-read user-library-read user-read-birthdate playlist-modify-private user-read-currently-playing user-read-recently-played user-follow-modify user-modify-playback-state user-read-playback-state user-follow-read user-library-modify streaming playlist-modify-public playlist-read-collaborative'],
            redirect_uri
        })
    )
})



app.get('/callback',(req,res) => {
    const code = req.query.code || null
    const authOptions = {
        url : 'https://accounts.spotify.com/api/token',
        form : {
          code: code,
          redirect_uri,
          grant_type: 'authorization_code'
        },
        headers : {
            'Authorization': 'Basic ' + (new Buffer(
                process.env.SPOTIFY_CLIENT_ID + ':' + process.env.SPOTIFY_CLIENT_SECRET
              ).toString('base64'))
        },
        json : true
    }
    request.post(authOptions,(err,response,body) => {
        const access_token = body.access_token
        const uri = process.env.FRONTEND_URI || 'http://localhost:3000'
        res.redirect(uri + '?access_token=' + access_token)
    })
})

app.listen(process.env.PORT || 8888,() => console.log('Spotify oauth bridge is running'))

